$(function(){
    
    var $mainMenuItems = $("#main-menu ul").children("li"),
        $totalMainMenuItems = $mainMenuItems.length,
        openedIndex = 2,
        init = function() {
           bindEvents();
           animateItem($mainMenuItems.eq(openedIndex),true,250);
           
        },
        bindEvents = function(){
             $mainMenuItems.children(".images").click(function(){
                // get the actuel item
                var newIndex = $(this).parent().index();
//                    $item = $mainMenuItems.eq(newIndex);
                checkAndAnimateItem(newIndex);
            });
            
            $(".btn").hover(
            function(){
                $(this).addClass("hovered")
            },
            function(){
                $(this).removeClass("hovered")  
            }
            );
            
            $(".btn").click(function(){
                var newIndex = $(this).index();
                $item = $mainMenuItems.eq(newIndex);
                checkAndAnimateItem(newIndex);
                
            });
        },
        animateItem = function($item, toOpen, speed){
                var $colorImage = $item.find(".colore"),
                    itemParam = toOpen ? {width:"420px"} : {width:"140px"},
                    colorImageParam = toOpen ? {left:"0px"} : {left:"140px"};
                
                $colorImage.animate(colorImageParam, speed);
                $item.animate(itemParam, speed);
            
        },
        checkAndAnimateItem = function(indexToCheckAndAnimate) {
            if (openedIndex === indexToCheckAndAnimate) {
                    
                     animateItem($mainMenuItems.eq(indexToCheckAndAnimate), false, 250);
                     openedIndex = -1;
                    
                } else {
                    animateItem($mainMenuItems.eq(openedIndex),false,250);

                    openedIndex = indexToCheckAndAnimate;
                    //get image color
                    animateItem($mainMenuItems.eq(openedIndex), true, 250);
                    
                }
        };
 
    init();
})